import React, { useState } from 'react';
import QRCode from 'qrcode.react';

import './App.css';

const App = () => {

    /*const generateQR = () => {
        let str = 'My First QR!'
        QRCode.toCanvas(document.getElementById('canvas'), str, (error) => {
            if (error) {
                console.log(error)
            }
        })
    }*/
    const [Name, setName] = useState('');
    const [lastName, setLastName] = useState('');
    const [text, setText] = useState('');
    const [size, setSize] = useState(256)

    const handleQr = (e) => {
        e.preventDefault();

        var aux = ''
        
        if (Name !== '' || lastName !== '') {
            var form = document.querySelector('form');
            var data = new FormData(form);
            for (var key of data.keys()) {
                console.log(data.get(key));
                aux += data.get(key);

            }
            console.log(aux)
            setText(aux);

        }
        if (Name.length === 0 || lastName.length === 0) {
            setText('')
        }
    }



    return (
        <div className="container">
            <form>
                <h1>QR Code Generator</h1>
                <div >
                    <label className="label1" >Name</label>
                    <input type='text' placeholder="Name" name="name" id="name" onChange={(e) => { setName(e.target.value) }} />
                </div>
                <div >
                    <label className="label2" >Last Name</label>
                    <input type='text' placeholder="Last Name" name="lastname" id="name" onChange={(e) => { setLastName(e.target.value) }} />
                </div>
                <button className="btn" onClick={handleQr}>Generate QR </button>
            </form>
            <div className="qrcode">
                {text === '' ? null : <QRCode value={text} size={size} fgColor='#3AB4D1' x='15' />}
            </div>

            {/* <canvas className="code" id="canvas"/>
            <button className="btn" onClick={generateQR} >
                 Generate QR Code
 </button> */}

        </div>


    )
}

export default App
